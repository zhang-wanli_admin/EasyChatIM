package com.zwl.gitForX.di.module

import com.zwl.gitForX.app.account.LoginActivity
import com.zwl.gitForX.app.account.RegisterActivity
import com.zwl.gitForX.app.account.UpdatePwdActivity
import com.zwl.gitForX.app.chat.ChatActivity
import com.zwl.gitForX.app.chat.GroupChatActivity
import com.zwl.gitForX.app.code.CodeActivity
import com.zwl.gitForX.app.code.ScanCodeActivity
import com.zwl.gitForX.app.friend.UserProfileActivity
import com.zwl.gitForX.app.group.GroupMemberActivity
import com.zwl.gitForX.app.group.GroupProfileActivity
import com.zwl.gitForX.app.home.HomeActivity
import com.zwl.gitForX.app.me.about.AboutActivity
import com.zwl.gitForX.app.me.user.ChangeUserInfoActivity
import com.zwl.gitForX.app.me.user.UserInfoActivity
import com.zwl.gitForX.app.photo.PhotoViewActivity
import com.zwl.gitForX.app.search.SearchActivity
import com.zwl.gitForX.app.splash.SplashActivity
import com.zwl.gitForX.app.web.WebActivity
import com.zwl.gitForX.temp.TempActivity
import com.king.frame.mvvmframe.di.component.BaseActivitySubcomponent
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * @author <a href="mailto:jenly1314@gmail.com">Jenly</a>
 */
@Module(subcomponents = [BaseActivitySubcomponent::class])
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeTempActivity(): TempActivity

    @ContributesAndroidInjector
    abstract fun contributeSplashActivity(): SplashActivity

    @ContributesAndroidInjector
    abstract fun contributeHomeActivity(): HomeActivity

    @ContributesAndroidInjector
    abstract fun contributeLoginActivity(): LoginActivity

    @ContributesAndroidInjector
    abstract fun contributeRegisterActivity(): RegisterActivity

    @ContributesAndroidInjector
    abstract fun contributeChatActivity(): ChatActivity

    @ContributesAndroidInjector
    abstract fun contributeGroupChatActivity(): GroupChatActivity

    @ContributesAndroidInjector
    abstract fun contributePhotoViewActivity(): PhotoViewActivity

    @ContributesAndroidInjector
    abstract fun contributeUserInfoActivity(): UserInfoActivity

    @ContributesAndroidInjector
    abstract fun contributeChangeUserInfoActivity(): ChangeUserInfoActivity

    @ContributesAndroidInjector
    abstract fun contributeAboutActivity(): AboutActivity

    @ContributesAndroidInjector
    abstract fun contributeUpdatePwdActivity(): UpdatePwdActivity

    @ContributesAndroidInjector
    abstract fun contributeSearchActivity(): SearchActivity

    @ContributesAndroidInjector
    abstract fun contributeUserProfileActivity(): UserProfileActivity

    @ContributesAndroidInjector
    abstract fun contributeGroupProfileActivity(): GroupProfileActivity

    @ContributesAndroidInjector
    abstract fun contributeCodeActivity(): CodeActivity

    @ContributesAndroidInjector
    abstract fun contributeScanCodeActivity(): ScanCodeActivity

    @ContributesAndroidInjector
    abstract fun contributeGroupMemberActivity(): GroupMemberActivity

    @ContributesAndroidInjector
    abstract fun contributeWebActivity(): WebActivity

}