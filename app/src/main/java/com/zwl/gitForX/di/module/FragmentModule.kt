package com.zwl.gitForX.di.module

import com.zwl.gitForX.app.friend.FriendFragment
import com.zwl.gitForX.app.group.GroupFragment
import com.zwl.gitForX.app.home.HomeFragment
import com.zwl.gitForX.app.me.MeFragment
import com.zwl.gitForX.temp.TempFragment
import com.king.frame.mvvmframe.di.component.BaseFragmentSubcomponent
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * @author <a href="mailto:jenly1314@gmail.com">Jenly</a>
 */
@Module(subcomponents = [BaseFragmentSubcomponent::class])
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeTempFragment(): TempFragment

    @ContributesAndroidInjector
    abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun contributeFriendFragment(): FriendFragment

    @ContributesAndroidInjector
    abstract fun contributeGroupFragment(): GroupFragment

    @ContributesAndroidInjector
    abstract fun contributeMeFragment(): MeFragment

}