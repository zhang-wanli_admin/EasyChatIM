package com.zwl.gitForX.di.component

import com.zwl.gitForX.App
import com.zwl.gitForX.di.module.ApplicationModule
import com.king.frame.mvvmframe.di.component.AppComponent
import com.king.frame.mvvmframe.di.scope.ApplicationScope
import dagger.Component

/**
 * @author <a href="mailto:jenly1314@gmail.com">Jenly</a>
 */
@ApplicationScope
@Component(dependencies = [AppComponent::class], modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun inject(app: App)
}