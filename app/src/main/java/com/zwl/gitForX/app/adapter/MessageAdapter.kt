package com.zwl.gitForX.app.adapter

import com.zwl.gitForX.BR
import com.zwl.gitForX.R
import com.zwl.gitForX.app.home.HomeViewModel
import com.zwl.gitForX.bean.Message
import com.zwl.gitForX.view.DragBubbleView

/**
 * @author <a href="mailto:jenly1314@gmail.com">Jenly</a>
 */
class MessageAdapter(val userId: String,val viewModel: HomeViewModel) : BindingAdapter<Message>(R.layout.rv_message_item) {

    var curTime =  System.currentTimeMillis()

    override fun convert(helper: BindingHolder, item: Message) {
        helper.getView<DragBubbleView>(R.id.dbvCount).setOnBubbleStateListener(object : DragBubbleView.OnBubbleStateListener{
            override fun onDrag() {
            }

            override fun onMove() {
            }

            override fun onRestore() {
            }

            override fun onDismiss() {
                when(item.messageMode){
                    Message.userMode -> viewModel.updateMessageRead(userId,item.id!!)
                    Message.groupMode -> viewModel.updateGroupMessageRead(userId,item.id!!)
                }
            }

        })

        helper.addOnClickListener(R.id.clContent)
        helper.addOnClickListener(R.id.llDelete)


        helper.mBinding?.let {
            it.setVariable(BR.curTime,curTime)
            it.setVariable(BR.data,item)
            it.executePendingBindings()
        }
    }
}