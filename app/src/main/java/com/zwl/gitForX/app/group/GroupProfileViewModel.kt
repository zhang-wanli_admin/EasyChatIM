package com.zwl.gitForX.app.group

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.zwl.gitForX.App
import com.zwl.gitForX.R
import com.zwl.gitForX.api.ApiService
import com.zwl.gitForX.bean.Group
import com.zwl.gitForX.bean.Result
import com.zwl.gitForX.netty.NettyClient
import com.zwl.gitForX.netty.packet.req.ApplyGroupReq
import com.zwl.gitForX.netty.packet.req.InviteGroupReq
import com.king.frame.mvvmframe.base.BaseModel
import com.king.frame.mvvmframe.base.DataViewModel
import com.king.frame.mvvmframe.base.livedata.StatusEvent
import com.king.frame.mvvmframe.http.callback.ApiCallback
import retrofit2.Call
import javax.inject.Inject

/**
 * @author <a href="mailto:jenly1314@gmail.com">Jenly</a>
 */
class GroupProfileViewModel @Inject constructor(application: Application, model: BaseModel?) : DataViewModel(application, model){

    val groupLiveData = MutableLiveData<Group>()

    /**
     * 获取群信息
     */
    fun getGroup(groupId: String){
        updateStatus(StatusEvent.Status.LOADING)
        val token = getApplication<App>().getToken()
        mModel.getRetrofitService(ApiService::class.java)
            .getGroup(token,groupId)
            .enqueue(object : ApiCallback<Result<Group>>(){
                override fun onResponse(call: Call<Result<Group>>?, result: Result<Group>?) {
                    result?.let {
                        if(it.isSuccess()){
                            updateStatus(StatusEvent.Status.SUCCESS)
                            groupLiveData.value = it.data
                        }else{
                            sendMessage(it.desc)
                            updateStatus(StatusEvent.Status.FAILURE)
                        }

                    } ?: run{
                        updateStatus(StatusEvent.Status.FAILURE)
                        sendMessage(R.string.result_failure)
                    }
                }

                override fun onError(call: Call<Result<Group>>?, t: Throwable?) {
                    updateStatus(StatusEvent.Status.ERROR)
                    sendMessage(t?.message)
                }

            })

    }

    /**
     * 邀请进群
     */
    fun inviteGroupReq(groupId: String,userId: String){
        val users = ArrayList<String>()
        users.add(userId)
        NettyClient.INSTANCE.sendMessage(InviteGroupReq(groupId,users))
    }

    /**
     * 申请加群
     */
    fun applyGroupReq(groupId: String){
        NettyClient.INSTANCE.sendMessage(ApplyGroupReq(groupId))
    }
}