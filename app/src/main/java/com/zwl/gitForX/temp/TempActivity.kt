package com.zwl.gitForX.temp

import android.os.Bundle
import androidx.databinding.ViewDataBinding
import com.zwl.gitForX.app.base.BaseActivity

/**
 * @author <a href="mailto:jenly1314@gmail.com">Jenly</a>
 */
class TempActivity : BaseActivity<TempViewModel, ViewDataBinding>(){

    override fun initData(savedInstanceState: Bundle?) {

    }

    override fun getLayoutId(): Int {
        return 0
    }

}