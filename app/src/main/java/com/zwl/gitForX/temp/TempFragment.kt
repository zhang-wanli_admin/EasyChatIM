package com.zwl.gitForX.temp

import android.os.Bundle
import androidx.databinding.ViewDataBinding
import com.zwl.gitForX.app.base.BaseFragment

/**
 * @author <a href="mailto:jenly1314@gmail.com">Jenly</a>
 */
class TempFragment : BaseFragment<TempViewModel, ViewDataBinding>(){

    companion object{
        fun newInstance(): TempFragment {
            return TempFragment()
        }
    }

    override fun initData(savedInstanceState: Bundle?) {
    }

    override fun getLayoutId(): Int {
        return 0
    }

}