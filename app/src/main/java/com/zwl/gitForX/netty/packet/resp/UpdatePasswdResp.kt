package com.zwl.gitForX.netty.packet.resp

import com.zwl.gitForX.netty.packet.Packet
import com.zwl.gitForX.netty.packet.PacketType

/**
 * @author Zed
 * date: 2019/08/19.
 * description:
 */
class UpdatePasswdResp : Packet() {
    var password: String? = null
    var success: Boolean = false
    var reason: String? = null

    override fun packetType(): Int {
        return PacketType.UPDATE_PASSWD_RESP
    }



}
