package com.zwl.gitForX.netty.handle

import com.zwl.gitForX.netty.packet.resp.HeartBeatResp
import io.netty.channel.ChannelHandler

/**
 * @author <a href="mailto:jenly1314@gmail.com">Jenly</a>
 */
@ChannelHandler.Sharable
class HeartBeatRespHandler : RespHandler<HeartBeatResp>(){
//    override fun channelRead0(ctx: ChannelHandlerContext?, msg: HeartBeatResp) {
//        Timber.d(msg.toString())
//    }
}