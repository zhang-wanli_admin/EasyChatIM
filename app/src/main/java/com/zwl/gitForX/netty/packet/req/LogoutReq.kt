package com.zwl.gitForX.netty.packet.req

import com.zwl.gitForX.netty.packet.Packet
import com.zwl.gitForX.netty.packet.PacketType

/**
 * @author Zed
 * date: 2019/10/09.
 * description:
 */
 class LogoutReq : Packet() {

    override fun packetType(): Int {
        return PacketType.LOGOUT_REQ
    }

    override fun toString(): String {
        return "LogoutReq(PacketType.LOGOUT_REQ)"
    }


}