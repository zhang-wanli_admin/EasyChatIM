package com.zwl.gitForX.netty.packet.req

import com.zwl.gitForX.netty.packet.Packet
import com.zwl.gitForX.netty.packet.PacketType

/**
 * @author <a href="mailto:jenly1314@gmail.com">Jenly</a>
 */
class SyncMessageReq : Packet(){

    override fun packetType(): Int {
        return PacketType.SYNC_MESSAGE_REQ
    }

    override fun toString(): String {
        return "SyncMessageReq(PacketType.SYNC_MESSAGE_REQ)"
    }

}