package com.zwl.gitForX.netty.packet.resp

import com.zwl.gitForX.netty.packet.Packet
import com.zwl.gitForX.netty.packet.PacketType

/**
 * @author <a href="mailto:jenly1314@gmail.com">Jenly</a>
 */
class AllowGroupResp(val groupId: String,val allow: Boolean) : Packet(){

    override fun packetType(): Int {
        return PacketType.ALLOW_GROUP_RESP
    }

    override fun toString(): String {
        return "AllowGroupResp(groupId='$groupId', allow=$allow)"
    }


}
