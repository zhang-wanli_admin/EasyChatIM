package com.zwl.gitForX.netty.packet.resp

import com.zwl.gitForX.netty.packet.Packet
import com.zwl.gitForX.netty.packet.PacketType

/**
 * @author <a href="mailto:jenly1314@gmail.com">Jenly</a>
 */
class ApplyGroupSelfResp(val groupId: String,var reason: String?,val success: Boolean) : Packet(){

    override fun packetType(): Int {
        return PacketType.APPLY_GROUP_SELF_RESP
    }

    override fun toString(): String {
        return "ApplyGroupSelfResp(groupId='$groupId', reason=$reason, success=$success)"
    }


}