package com.zwl.gitForX.netty.packet.resp

import com.zwl.gitForX.netty.packet.Packet
import com.zwl.gitForX.netty.packet.PacketType

/**
 * @author <a href="mailto:jenly1314@gmail.com">Jenly</a>
 */
class ApplyGroupResp(val applyUserId: String,val applyUserName: String,val groupId: String,val groupName: String) : Packet(){

    override fun packetType(): Int {
        return PacketType.APPLY_GROUP_RESP
    }

    override fun toString(): String {
        return "ApplyGroupResp(applyUserId='$applyUserId', applyUserName='$applyUserName', groupId='$groupId')"
    }

}