package com.zwl.gitForX.netty.packet.req

import com.zwl.gitForX.netty.packet.Packet
import com.zwl.gitForX.netty.packet.PacketType

/**
 * @author <a href="mailto:jenly1314@gmail.com">Jenly</a>
 */
class ApplyGroupReq(val groupId: String) : Packet(){
    override fun packetType(): Int {
        return PacketType.APPLY_GROUP_REQ
    }

    override fun toString(): String {
        return "ApplyGroupReq(groupId='$groupId')"
    }

}