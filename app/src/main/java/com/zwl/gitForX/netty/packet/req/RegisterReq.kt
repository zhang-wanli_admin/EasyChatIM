package com.zwl.gitForX.netty.packet.req

import com.zwl.gitForX.netty.packet.Packet
import com.zwl.gitForX.netty.packet.PacketType

/**
 * @author Zed
 * date: 2019/10/09.
 * description:
 */
 class RegisterReq(val userName: String, val password: String,val login: Boolean = true) : Packet() {

    override fun packetType(): Int {
        return PacketType.REGISTER_REQ
    }

    override fun toString(): String {
        return "RegisterReq(userName='$userName', password='$password', login=$login)"
    }


}