package com.zwl.gitForX.netty.packet.req

import com.zwl.gitForX.netty.packet.Packet
import com.zwl.gitForX.netty.packet.PacketType

/**
 * @author Zed
 * date: 2019/10/09.
 * description:
 */
class CreateGroupReq(val groupName : String, val users : List<String>?) : Packet(){
    override fun packetType(): Int {
        return PacketType.CREATE_GROUP_REQ
    }

    override fun toString(): String {
        return "CreateGroupReq(groupName='$groupName', users=$users)"
    }

}