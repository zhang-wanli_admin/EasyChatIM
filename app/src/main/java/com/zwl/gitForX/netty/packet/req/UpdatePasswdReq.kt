package com.zwl.gitForX.netty.packet.req

import com.zwl.gitForX.netty.packet.Packet
import com.zwl.gitForX.netty.packet.PacketType

/**
 * @author Zed
 * date: 2019/10/09.
 * description:
 */
 class UpdatePasswdReq(val oldPassword: String, val newPassword: String) : Packet() {

    override fun packetType(): Int {
        return PacketType.UPDATE_PASSWD_REQ
    }

    override fun toString(): String {
        return "UpdatePasswdReq(oldPassword='$oldPassword', newPassword='$newPassword')"
    }


}