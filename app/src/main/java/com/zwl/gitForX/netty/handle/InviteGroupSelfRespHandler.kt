package com.zwl.gitForX.netty.handle

import com.zwl.gitForX.netty.packet.resp.InviteGroupSelfResp
import io.netty.channel.ChannelHandler

/**
 * @author Zed
 * date: 2019/10/12.
 * description:
 */
@ChannelHandler.Sharable
class InviteGroupSelfRespHandler : RespHandler<InviteGroupSelfResp>()