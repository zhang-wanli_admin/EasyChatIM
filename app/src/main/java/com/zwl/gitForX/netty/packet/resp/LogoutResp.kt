package com.zwl.gitForX.netty.packet.resp

import com.zwl.gitForX.netty.packet.Packet
import com.zwl.gitForX.netty.packet.PacketType

/**
 * @author Zed
 * date: 2019/08/19.
 * description:
 */
class LogoutResp : Packet() {

    override fun packetType(): Int {
        return PacketType.LOGOUT_RESP
    }



}
