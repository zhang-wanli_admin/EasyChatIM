package com.zwl.gitForX.bean

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * @author <a href="mailto:jenly1314@gmail.com">Jenly</a>
 */
@Parcelize
class Operator(val event: Int): Parcelable